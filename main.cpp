#include <stdlib.h>
#include <GL/glut.h>
#include <GL/glu.h>
#include <iostream>

using namespace std;
GLUquadric *qobj;
GLUquadricObj *quadric;

void init(void) {
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_NORMALIZE);

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_COLOR_MATERIAL);

    glShadeModel(GL_SMOOTH);
    qobj = gluNewQuadric();
    //quadric = gluNewQuadric();
    gluQuadricNormals(qobj, GLU_SMOOTH);
}

void display(void) {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    gluLookAt(0.0, 1.75, 1.f, 0.0, 0.0, -1.0, 0.0, 1.0, 0.0);
    glTranslatef(0.f, 0.25f, 0.f);

    //draw a table
    glColor3ub(140, 50, 65);

    glPushMatrix();
    glTranslatef(-.9f, -0.8f, -.6f);
    glScalef(0.1f, 1.f, 0.1f);
    glutSolidCube(1.f);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(+.9f, -0.8f, -.6f);
    glScalef(0.1f, 1.f, 0.1f);
    glutSolidCube(1.f);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(0.0f, -0.40f, -1.5f);
    glScalef(3.f, 0.05f, 2);
    glutSolidCube(1.f);
    glPopMatrix();

    // Яблоко
    glPushMatrix();
    glColor3ub(141, 182, 0);
    glTranslatef(-0.6f, 0.1f, -.6f);
    glScalef(0.0458f, 0.05f, 0.05f);
    glutSolidSphere(3.0f, 100, 100);
    glPopMatrix();

    // помидор
    glPushMatrix();
    glColor3ub(168, 10, 16);
    glTranslatef(0, 0.1f, -.6f);
    glScalef(0.06, 0.045, 0.05f);
    glutSolidSphere(3.0f, 100, 100);
    glPopMatrix();

    // Веточки помидора
    glPushMatrix();
    glColor3ub(24, 68, 26);
    glRotated(2, 1, 0, 0);
    glTranslatef(0, 0.3f, -0.6f);
    glScalef(0.2f, 1.f, 0.1f);
    glutSolidCube(0.1f);
    glPopMatrix();

    glPushMatrix();
    glColor3ub(24, 68, 26);
    glRotated(20, 0, 0, 0.5f);
    glTranslatef(0.08f, 0.3f, -0.6f);
    glScalef(0.2f, 1.f, 0.1f);
    glutSolidCube(0.1f);
    glPopMatrix();

    glPushMatrix();
    glColor3ub(24, 68, 26);
    glRotated(-20, 0, 0, 0.5f);
    glTranslatef(-0.06f, 0.3f, -0.6f);
    glScalef(0.2f, 1.f, 0.1f);
    glutSolidCube(0.1f);
    glPopMatrix();

    // Корнишон
    glPushMatrix();
    glColor3ub(75, 148, 80);
    glTranslatef(+0.6f, 0.1f, -.6f);
    glScalef(-0.3, 0.1, 0.05f);
    glutSolidSphere(.5f, 100, 100);
    glPopMatrix();

    // Веточка корнишона
    glPushMatrix();
    glColor3ub(24, 68, 26);
    glTranslatef(+0.47f, 0.1f, -0.6f);
    glScalef(1, .1f, 0.1f);
    glutSolidCube(0.1f);
    glPopMatrix();

    // Веточка
    glPushMatrix();
    glColor3ub(64, 50, 16);
    glTranslatef(-0.6, 0.3f, -0.6f);
    glScalef(0.2f, 1.f, 0.1f);
    glutSolidCube(0.1f);
    glPopMatrix();

    ////draw a floor
    glColor3ub(198, 194, 150);
    glPushMatrix();
    glTranslatef(0.0f, -3.0f, 0.0f);
    glScalef(15.f, 0.1f, 25.f);
    glutSolidCube(1.f);
    glPopMatrix();
    glutSwapBuffers();
}

void reshape(int w, int h) {
    glViewport(0, 0, (GLsizei) w, (GLsizei) h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0, (GLfloat) w / (GLfloat) h, 1.0, 20.0);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

}

void keyboard(unsigned char key, int x, int y) {
    switch (key) {
        //turn on/off lights
        case 'q':
        case 'Q':
            gluDeleteQuadric(qobj);
            exit(0);
            break;
    }
    glutPostRedisplay();
}

int main(int argc, char **argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(800, 800);
    glutInitWindowPosition(0, 0);
    glutCreateWindow(argv[0]);
    init();
    glutDisplayFunc(display);
    glutIdleFunc(display);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(keyboard);
    glutMainLoop();
    return 0;
}